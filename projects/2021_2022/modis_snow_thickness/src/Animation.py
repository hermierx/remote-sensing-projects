import sys
import Main_Program_to_Execute as m
import PIL


def avec_nuages(liste_day, liste_month, nuages = True, cropped = '5', nom = 'new_ani'):
    """
    Fonction qui va generer les images correspondant aux dates demandées en fonction de certaines spécifications
    Input : 
        liste_day : Liste de caractères correspondant aux jours étudiés (attention, si traitement des nuages ne pas mettre 1)
        liste_mois : Liste de caractères des mois des jours précédents
        nuages : True si traitement des nuages, False sinon
        cropped : '3' si vue globale, '5' si vue sur l'Isère
    Output :
        liste_nom : La liste des noms des images pour la future création de GIF
    """
    GEO_ZONE = 'Alps'
    liste_nom = []
    for i in range(len(liste_day)) :
        if cropped:
            selected_product, file_type, file_name, exist = m.select_file(liste_day[i], liste_month[i], GEO_ZONE, cropped)  #Then the type of file and if we need to download it
        else:
            selected_product, file_type, file_name, exist = m.select_file(liste_day[i], liste_month[i], GEO_ZONE, cropped)
        #if the selected_product doesn't in the main folder, download it
        if not exist:
            file_name = m.download_hdf_file(cropped, liste_month[i], liste_day[i], GEO_ZONE, username, password)
        else : 
            print("\nYou already have this file in your folder, the program won't download it again.\n")
                #Verify the download is over + loop to stop the execution if not found
        if not m.downloaded(file_name):
            print("\nThe download has failed. \n")
            sys.exit()


        if nuages:
            day2 = int(liste_day[i])-1

            if day2 <= 0:
                print("Error, incorect date")
                sys.exit()
            day1 = str(day2)


            selected_product1, file_type1, file_name1, exist1 = m.select_file(day1, liste_month[i], GEO_ZONE, cropped)  #Then the type of file and if we need to download it
            #if the selected_product doesn't in the main folder, download it
            if not exist1 : 
                file_name1 = m.download_hdf_file(cropped, liste_month[i], day1, GEO_ZONE, username, password)
            else : 
                print("\nYou already have this file in your folder, the program won't download it again.\n")
            #Verify the download is over + loop to stop the execution if not found
            if not m.downloaded(file_name1):
                print("\nThe download has failed. \n")
                sys.exit()

            if cropped == '3':
                m.cs.compute_and_save_MOD10A1_clouds(file_name, file_name1, liste_month[i], liste_day[i], day1)
            else:
                m.cs.compute_and_save_MOD10A1_cropped_clouds(file_name, file_name1, liste_month[i], liste_day[i], day1)
        else:
            m.compute_and_save(cropped,file_name,liste_month[i],liste_day[i],GEO_ZONE)
        print("________________________________________________________________\n")
        print("The files have been succesfully processed ! \n")
        print("They are available in the tel-detect folder, where you empty the .zip from gitlab.\n")
        liste_nom.append(file_name)
        m.delete_tif()
        
    images = []
    for i in range(len(liste_day)) :
        if cropped == '5' :
            im = PIL.Image.open('500m_Focus_' + liste_nom[i][:-3] + 'png')
        else : 
            im = PIL.Image.open('500m_' + liste_nom[i][:-3] + 'png')
        images.append(im)
    
    animation_name = nom + '.gif'
    images[0].save(animation_name,
            save_all = True, append_images = images[1:], 
            optimize = False, duration = 30)
    print("Création de l'animation terminée")
    return None


        