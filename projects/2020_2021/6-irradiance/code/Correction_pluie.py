A=1
B=2

import numpy as np
from skimage import *
import matplotlib.pyplot as plt
from math import sqrt


def im_moyenee(im_origine):
    im=np.copy(im_origine)/255
    lx=im.shape[0]
    ly=im.shape[1]
    for i in range(lx):
        for j in range(ly):
            (r,v,b)=im[i][j]
            im[i][j]=(0.3*(r+v+b),0,0)
    return im


masque=io.imread('C://Users//naori//Documents//Cours e3//télédetection//pluie//masque.jpg')/255 #en 01

#calcule la valeur moyenne, l'équart type et la moyenne sur chaque couche (rvb)
def moy_sigma(img_origine):
    im=img_origine/255
    lx=im.shape[0]
    ly=im.shape[1]
    X=[]
    R=0
    V=0
    B=0
    S=0
    n=0
    sigma=0

    for i in range(lx):
        for j in range(ly):
            (r,v,b)=im[i][j]
            if (i-lx/2)**2+(j-ly/2)**2<(860**2) and 0.3*(r+v+b) >0.26:
                X.append(0.3*(r+v+b))
                R+=r
                V+=v
                B+=b
                S+=0.3*(r+v+b)
                n+=1
    m=S/n
    mr=R/n
    mv=V/n
    mb=B/n
    for i in range(len(X)):
        sigma+=(X[i]-m)**2
    return (m,sqrt(sigma/n),mr,mv,mb)

#renvoie 1 si des gouttes sont détectées 0 sinon
def test_pluie(im_origine): #entree=image 0/255 3 couche #sortie 01 1 couche
    (moy,sigma,mr,mv,mb)=moy_sigma(im_origine)
    im=im_origine/255
    lx=im.shape[0]
    ly=im.shape[1]
    seuil_pluie=2300
    im_s=np.zeros((lx,ly))
    pixels_gouttes=0
    for i in range(lx):
        for j in range(ly):
            if (i-lx/2)**2+(j-ly/2)**2<(860**2) and 0.3*(im[i][j][0]+im[i][j][1]+im[i][j][2])<A*moy-B*sigma :
                pixels_gouttes+=1
                im_s[i][j]=1.
    if pixels_gouttes<seuil_pluie:
        return 0
    return 1
   # return (im_s,pixels_gouttes)


#renvoie l'image corrigée
def correction_pluie (im_origine):
    (moy,sigma,mr,mv,mb)=moy_sigma(im_origine)
    im=im_origine/255
    lx=im.shape[0]
    ly=im.shape[1]
    im_loc_pluie=np.zeros((lx,ly,3))
    for i in range(lx):
        for j in range(ly):
            if (i-lx/2)**2+(j-ly/2)**2<(920**2) and 0.3*(im[i][j][0]+im[i][j][1]+im[i][j][2])<A*moy-B*sigma :
                im_loc_pluie[i][j]=(mr,mv,mb)
                im[i][j]=0.
    im=im+im_loc_pluie
    return im

#module de lecture
img1=io.imread('C://Users//naori//Documents//Cours e3//télédetection//novembre//novembre (210).jpg')
img2=io.imread('C://Users//naori//Documents//Cours e3//télédetection//novembre//novembre (178).jpg')
#img3=io.imread('C://Users//naori//Documents//Cours e3//télédetection//novembre//novembre (178).jpg')
#print(moy_sigma(img1)[0])
#io.imshow(correction_pluie(img2))
io.imsave('corr1.png', correction_pluie(img1))
plt.show()