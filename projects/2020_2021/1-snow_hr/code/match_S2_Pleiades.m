%% Mise en correspondance d'une image Sentinel-2 et Pl�iades

%%
info_S2=geotiffinfo(''); %Mettre ici l'acc�s vers une tuile Sentinel-2 � la r�solution de 20m

%% Chargement des donn�es Pl�iade
P=double(imread('../data/Pleiades/Zone1_fused.tif')); % Charger l'image Pl�iade
info_P9=geotiffinfo('../data/Pleiades/Zone1_fused.tif'); % Infos de g�or�f�rencement de l'image Pl�iade
%% Mise en correspondance entre les images

info_P9.PixelScale(1)=0.5;% Taille du pixel Pl�iades
info_P9.PixelScale(2)=0.5;% Taille du pixel Pl�iades
rows_source= info_S2.Height;
cols_source=info_S2.Width;
[rows_P,cols_P,~]=size(P);

limhx=floor(ceil((info_P9.CornerCoords.X(1)-info_S2.CornerCoords.X(1))/info_S2.PixelScale(2)));
limhy=floor(ceil(((info_S2.CornerCoords.Y(1))-info_P9.CornerCoords.Y(1))/info_S2.PixelScale(1)));
debutSpotx=((info_P9.CornerCoords.X(1)-(info_S2.CornerCoords.X(1)))/info_S2.PixelScale(2))-floor((info_P9.CornerCoords.X(1)-(info_S2.CornerCoords.X(1)))/info_S2.PixelScale(1));

debutSpotx=floor(ceil(debutSpotx*info_S2.PixelScale(1)/info_P9.PixelScale(1)));
debutSpoty=(((info_P9.CornerCoords.Y(1)-(info_S2.CornerCoords.Y(1)))/info_S2.PixelScale(2))-floor((info_P9.CornerCoords.Y(1)-(info_S2.CornerCoords.Y(1)))/info_S2.PixelScale(1)));
debutSpoty=floor(ceil(debutSpoty*info_S2.PixelScale(2)/info_P9.PixelScale(2)));

limbx=floor(floor(limhx+((cols_P-debutSpotx)*info_P9.PixelScale(1)/info_S2.PixelScale(1))));
limby=floor(limhy+(floor((rows_P-debutSpoty)*info_P9.PixelScale(2)/info_S2.PixelScale(2))));

finSpotx=debutSpotx+floor((limbx-limhx)*info_S2.PixelScale(2)/info_P9.PixelScale(2));
finSpoty=debutSpoty+floor((limby-limhy)*info_S2.PixelScale(2)/info_P9.PixelScale(2));



%% Chargement des images Pl�iade sur la bonne zone
[HS,MS]=l_source_S2_ms_hs(limhy,limby-1,limhx,limbx-1);

%% Coupure de Pl�iade � la bonne zone
Pleiades=P(debutSpoty:finSpoty-1,debutSpotx:finSpotx-1,:);




